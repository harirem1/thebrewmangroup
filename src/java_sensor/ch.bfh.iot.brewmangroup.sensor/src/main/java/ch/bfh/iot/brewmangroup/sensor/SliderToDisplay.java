import com.tinkerforge.*;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class SliderToDisplay {
    private static final String HOST = "localhost";
    private static final int PORT = 4223;

    // Change XYZ to the UID of your Segment Display 4x7 Bricklet
    private static final String CO2Sensor = "x71";
    private static final String SegmentDisplay = "pN7";

    private static final int Multiplikator = 6515/100;


    // Note: To make the example code cleaner we do not handle exceptions. Exceptions
    //       you might normally want to catch are described in the documentation
    public static void main(String args[]) throws Exception {

        IPConnection ipcon = new IPConnection(); // Create IP connection
        BrickletCO2 Co2Bricklet =
                new BrickletCO2(CO2Sensor, ipcon); // Create device object
        ipcon.connect(HOST, PORT); // Connect to brickd
        // Don't use device before ipcon is connected
        Co2Bricklet.setCO2ConcentrationCallbackPeriod(10000);
        Co2Bricklet.addCO2ConcentrationListener(e -> {
            try {
                System.out.println(Co2Bricklet.getCO2Concentration());
                String message = "value: "+Co2Bricklet.getCO2Concentration();
                MQTTDemo.publish(message, CO2Sensor);
            } catch (TimeoutException e1) {
                e1.printStackTrace();
            } catch (NotConnectedException e1) {
                e1.printStackTrace();
            }
        });



        //System.out.println("Press key to exit");
        System.in.read();
        ipcon.disconnect();
    }



    public static class MQTTDemo {



        public MQTTDemo() {}

        public static void main(String[] args) {

        }

        public static void publish(String valueToPublish, String deviceID) {
            try {
                MqttClient client;
                MqttConnectOptions options = new MqttConnectOptions();
                options.setPassword("1234".toCharArray());
                options.setUserName("mqttclient");
                client = new MqttClient("tcp://147.87.116.57", "ibidrgaebu");

                client.connect(options);
                MqttMessage message = new MqttMessage();
                message.setPayload(valueToPublish.getBytes());
                client.publish("tf/co2/u/"+deviceID+"/e/", message);
                client.disconnect();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }
}

