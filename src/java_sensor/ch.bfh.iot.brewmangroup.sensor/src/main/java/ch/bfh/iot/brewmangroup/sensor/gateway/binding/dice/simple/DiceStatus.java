/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bfh.iot.brewmangroup.sensor.gateway.binding.dice.simple;

import ch.quantasy.mqtt.gateway.client.message.AStatus;
import ch.quantasy.mqtt.gateway.client.message.annotations.Range;

/**
 *
 * @author reto
 */
public class DiceStatus extends AStatus{
    @Range(from = 0, to = 10006)
    public int sides;

    public DiceStatus(int sides) {
        this.sides = sides;
    }

    private DiceStatus() {
    }
    
}
