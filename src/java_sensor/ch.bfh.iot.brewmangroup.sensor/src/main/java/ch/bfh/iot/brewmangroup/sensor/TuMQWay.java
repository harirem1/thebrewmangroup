/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bfh.iot.brewmangroup.sensor;

import ch.bfh.iot.brewmangroup.sensor.gateway.binding.dice.simple.DiceStatus;
import ch.bfh.iot.brewmangroup.sensor.gateway.binding.dice.simple.SimpleDiceServiceContract;
import java.io.IOException;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import ch.quantasy.mqtt.gateway.client.GatewayClient;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * This is a convenience class that starts the Services and the Servant. Please
 * note, that these Classes could be started within their own processes even on
 * different computers. The only important thing is, that they connect to the
 * same MQTT-broker instance.
 *
 * @author reto
 */
public class TuMQWay {

    private static String computerName;

    static {
        try {
            computerName = java.net.InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            Logger.getLogger(TuMQWay.class.getName()).log(Level.SEVERE, null, ex);
            computerName = "undefined";
        }
    }

    public static void main(String[] args) throws MqttException, InterruptedException, IOException {
        URI mqttURI = URI.create("tcp://147.87.116.57:1883");
        if (args.length > 0) {
            mqttURI = URI.create(args[0]);
        } else {
            System.out.printf("Per default, 'tcp://147.87.116.57:1883' is chosen.\nYou can provide another address as first argument i.e.: tcp://iot.eclipse.org:1883\n");
        }

        System.out.printf("\n%s will be used as broker address.\n", mqttURI);
        GatewayClient<SimpleDiceServiceContract> gatewayClient;

        gatewayClient = new GatewayClient<>(mqttURI, computerName, new SimpleDiceServiceContract("co2"));
        gatewayClient.connect();
        //TODO Add new Status like DiceStatus
        gatewayClient.readyToPublish("tf/co2/", new DiceStatus(13223));
        System.out.println("intetnt published");
        System.in.read();
    }
}
