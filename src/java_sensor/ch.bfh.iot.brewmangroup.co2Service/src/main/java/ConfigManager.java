import javax.swing.text.html.Option;
import java.io.*;
import java.util.Optional;
import java.util.Properties;

public class ConfigManager {

    private Properties prop;
    private InputStream input;
    private final String PROPERTYFILE = "config.properties";

    private String currenHost = "localhost";
    private int currentPort = 4223;
    private int currentInterval = 600000;
    private String currenSensorID = "x71";

    private static final String hostpropName = "HOST";
    private static final String portpropName = "PORT";
    private static final String intervalpropName = "INTERVAL";
    private static final String sensorIdpropName = "SENSORID";

    ConfigManager() {
        prop = new Properties();
        if (!new File(PROPERTYFILE).exists()) {
            CreatePropertyFile();
        }
        else {
            try {

                input = new FileInputStream(PROPERTYFILE);
                prop.load(input);
                currenHost = Optional.ofNullable(prop.getProperty(hostpropName)).orElse(currenHost);
                currentPort = Integer.parseInt(Optional.ofNullable((prop.getProperty(portpropName))).orElse(String.valueOf(currentPort)));
                currentInterval = Integer.parseInt(Optional.ofNullable(prop.getProperty(intervalpropName)).orElse(String.valueOf(currentInterval)));
                currenSensorID = Optional.ofNullable(prop.getProperty(sensorIdpropName)).orElse(currenSensorID);
                CreatePropertyFile();
            } catch (IOException ex) {
                System.out.println("Error reading Config" + ex.toString());
            } finally {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }

    public void CreatePropertyFile() {
        OutputStream output = null;
        try {
            output = new FileOutputStream(PROPERTYFILE);
            prop.setProperty(hostpropName, currenHost);
            prop.setProperty(portpropName, String.valueOf(currentPort));
            prop.setProperty(intervalpropName, String.valueOf(currentInterval));
            prop.setProperty(sensorIdpropName, currenSensorID);
            prop.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public String getCurrenHost() {
        return currenHost;
    }

    public void setCurrenHost(String currenHost) {
        this.currenHost = currenHost;
    }

    public int getCurrentPort() {
        return currentPort;
    }

    public void setCurrentPort(int currentPort) {
        this.currentPort = currentPort;
    }

    public int getCurrentInterval() {
        return currentInterval;
    }

    public void setCurrentInterval(int currentInterval) {
        this.currentInterval = currentInterval;
    }

    public String getCurrenSensorID() {
        return currenSensorID;
    }

    public void setCurrenSensorID(String currenSensorID) {
        this.currenSensorID = currenSensorID;
    }

}
