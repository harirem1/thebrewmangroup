import com.tinkerforge.*;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class BrewmanService {


    public static void main(String args[]) throws Exception {

        ConfigManager config = new ConfigManager();
        IPConnection ipcon = new IPConnection(); // Create IP connection
        BrickletCO2 Co2Bricklet = new BrickletCO2(config.getCurrenSensorID(), ipcon); // Create device object
        ipcon.connect(config.getCurrenHost(), config.getCurrentPort()); // Connect to brickd
        Co2Bricklet.setCO2ConcentrationCallbackPeriod(config.getCurrentInterval());
        Co2Bricklet.addCO2ConcentrationListener(e -> {
            try {
                String message = "value: " + Co2Bricklet.getCO2Concentration();
                MQTTClient.publish(message, config.getCurrenSensorID());
            } catch (TimeoutException e1) {
                e1.printStackTrace();
            } catch (NotConnectedException e1) {
                e1.printStackTrace();
            }
        });
        while (true) ;
    }

    public static class MQTTClient {

        public MQTTClient() {}

        public static void publish(String valueToPublish, String deviceID) {
            try {
                MqttClient client;
                MqttConnectOptions options = new MqttConnectOptions();
                options.setPassword("1234".toCharArray());
                options.setUserName("mqttclient");
                client = new MqttClient("tcp://147.87.116.57", "mqttClient");
                client.connect(options);
                MqttMessage message = new MqttMessage();
                message.setPayload(valueToPublish.getBytes());
                client.publish("tf/co2/u/"+deviceID+"/e/", message);
                client.disconnect();
            } catch (MqttException e) {
                //Logger
            }
        }
    }
}

