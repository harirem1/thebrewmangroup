### Installation 

* Install needed modules by running `pip install <module>`
* Setup conf file: 

```
---
broker_ip: ""
broker_port: "1883"
broker_username: "username"
broker_password: "password"
broker_subscription: 'topic'


firebase_url: "url"
firebase_secret: "secret"
firebase_email: "mail"
```
