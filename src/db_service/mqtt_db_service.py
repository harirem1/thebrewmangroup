#!/usr/bin/env python 

# brutl1 
import paho.mqtt.client as mqtt
from firebase import firebase
import yaml
import sys
import datetime 
import logging


with open('mqtt_db_service.yaml', 'r') as f: 
    config = yaml.load(f)


root = logging.getLogger()
root.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)
root.info('started logger')



# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    root.info("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(config["broker_subscription"])

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):

    root.info('received message')
    root.debug('msg topic: {}'.format(msg.topic))
    root.debug('msg payload: {}'.format(msg.payload))

    topic = msg.topic.split('/')
    payload = yaml.load(msg.payload)
    

    write_db(topic, payload)



def write_db(topic, payload): 

    # broker_subscription = 'tf/co2/u/+/e/#'

    """
    datastructure in firebase database: 

    /sensors/<id>
                type: co2
                data: 
                    [time: 123, value: 1]
    """

    #payload = { "12345667": { "id": "123", "value": "987"} }
    #payload = { payload["timestamp"]: {"value": payload["value"] }}
    now = datetime.datetime.now()
    payload = { "timestamp": now, "type": topic[1], "value": payload["value"]}
      
    fb.post('/sensors/{}'.format(topic[3]), payload)

    root.info('successfully send message to firebase')
    #fb.put('/sensors/', topic[3], payload)


def main():
    # if its hacky and you know it clap your hands
    # (clap, clap)
    global fb

    client = mqtt.Client()
    client.username_pw_set(config["broker_username"], config["broker_password"])
    client.on_connect = on_connect
    client.on_message = on_message

    try: 
        client.connect(config["broker_ip"], config["broker_port"], 60)
    except: 
        sys.exit("MQTT connection to {}:{} failed".format(config["broker_ip"], config["broker_port"])) 

    try: 
        #firebase stuff
        authentication = firebase.FirebaseAuthentication(config["firebase_secret"],config["firebase_email"], True, True)
        fb = firebase.FirebaseApplication(config["firebase_url"], authentication)
    except: 
        sys.exit("Firebase connection to {} failed".format(config["firebase_url"]))

    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a
    # manual interface.
    client.loop_forever()

if __name__ == "__main__":
    main()

