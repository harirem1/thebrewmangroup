package com.example.thebrewmangroup.brewmaster;

import android.arch.core.util.Function;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.example.thebrewmangroup.brewmaster.Entity.BrewIteration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class BrewFragment extends Fragment {
    private ListView listView;
    private BrewIterationService brewIterationService = new BrewIterationService("myuser");
    private ArrayAdapter arrayAdapter2;
    private List<BrewIteration> data = new ArrayList<>();


    public BrewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    public static BrewFragment newInstance(BrewIteration brewIteration) {
        BrewFragment fragment = new BrewFragment();
        return fragment;
    }


    public static BrewFragment newInstance() {
        BrewFragment fragment = new BrewFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_brew, container, false);
        // Inflate the layout for this fragmen
        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BrewIteration iteration = new BrewIteration();
                //iteration.setStartDate(new Date());
                getFragmentManager().beginTransaction()
                        .replace(R.id.bodyFragment, BrewIterationDetail.newInstance(iteration))
                        .commit();
            }
        });
        this.brewIterationService = new BrewIterationService("myuser");
        this.listView =  (ListView) v.findViewById(R.id.brewIterationList);
        BrewIterationAdapter myAdapter = new BrewIterationAdapter(getContext(), data);
        data = brewIterationService.GetBrewIterations(myAdapter);
        listView.setAdapter(myAdapter);

        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BrewIteration selectedItem = (BrewIteration) parent.getItemAtPosition(position);
                getFragmentManager().beginTransaction()
                        .replace(R.id.bodyFragment, BrewIterationDetail.newInstance(selectedItem ))
                        .commit();
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }




    @Override
    public void onDetach() {
        super.onDetach();
    }



    public interface OnFragmentInteractionListener {
    }
}
