package com.example.thebrewmangroup.brewmaster;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.thebrewmangroup.brewmaster.Entity.BrewIteration;

import java.util.ArrayList;
import java.util.List;

public class BrewIterationAdapter extends ArrayAdapter<BrewIteration>{
    private List<BrewIteration> brewIterations;
    private LayoutInflater layoutInflater;

    public BrewIterationAdapter(Context aContext, List<BrewIteration> brewIterations) {
        super(aContext, 0, brewIterations);
        this.brewIterations = brewIterations;
        this.layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return brewIterations.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BrewIteration iteration = getItem(position);
        if (convertView == null )
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row_layout, parent, false);
        }
        TextView titleView = (TextView) convertView.findViewById(R.id.title);
        TextView descriptionView = (TextView) convertView.findViewById(R.id.description);
        TextView iterationDateView = (TextView) convertView.findViewById(R.id.date);

        titleView.setText(iteration .getName());
        descriptionView.setText(iteration.getDescription());
        if(iteration.getStartDate() != null) {
            iterationDateView.setText(iteration.getStartDate().toString());
        }
        return convertView;
    }
}
