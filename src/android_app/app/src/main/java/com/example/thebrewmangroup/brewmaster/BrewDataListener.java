package com.example.thebrewmangroup.brewmaster;

import com.example.thebrewmangroup.brewmaster.Entity.BrewData;
import com.example.thebrewmangroup.brewmaster.Entity.BrewIteration;

import java.util.List;

public interface BrewDataListener {
    public void recieveData(BrewIteration iteration, List<BrewData> data);
}
