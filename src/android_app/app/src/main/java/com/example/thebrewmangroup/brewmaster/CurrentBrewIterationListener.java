package com.example.thebrewmangroup.brewmaster;

import com.example.thebrewmangroup.brewmaster.Entity.BrewIteration;

import java.util.List;

public interface CurrentBrewIterationListener {
    public void recieveCurrentBrewIteration(BrewIteration iteration);
}
