package com.example.thebrewmangroup.brewmaster;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import com.example.thebrewmangroup.brewmaster.Entity.BrewIteration;


public class MainActivity extends AppCompatActivity implements BrewFragment.OnFragmentInteractionListener, BrewIterationDetail.OnFragmentInteractionListener {

    private BottomNavigationView navigation;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            Fragment fragment = new BrewIterationDetail();
            FragmentManager fragmentManager = getSupportFragmentManager();



            switch (item.getItemId()) {
                case R.id.navigation_current:
                    fragment = new BrewIterationDetail();
                    navigation.getMenu().findItem(R.id.navigation_current).setChecked(true);
                    navigation.getMenu().findItem(R.id.navigation_history).setChecked(false);
                    break;
                case R.id.navigation_history:
                    fragment = new BrewFragment();
                    navigation.getMenu().findItem(R.id.navigation_history).setChecked(true);
                    navigation.getMenu().findItem(R.id.navigation_current).setChecked(false);
                    break;
            }

            fragmentManager.beginTransaction()
                    .replace(R.id.bodyFragment, fragment)
                    .commit();
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().add(R.id.bodyFragment, new BrewIterationDetail()).commit();
        this.navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }


}
