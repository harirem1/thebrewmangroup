package com.example.thebrewmangroup.brewmaster.Entity;

import java.util.Date;

public class BrewData {

    private String type;
    private Date timestamp;
    private int value;

    public Date getTimestamp() {
        return timestamp;
    }

    /*public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }*/
    public void setTimestamp(Long date) {this.timestamp = new Date(date);}

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
