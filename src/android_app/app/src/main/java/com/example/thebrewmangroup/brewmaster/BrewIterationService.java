package com.example.thebrewmangroup.brewmaster;

import android.widget.BaseAdapter;

import com.example.thebrewmangroup.brewmaster.Entity.BrewData;
import com.example.thebrewmangroup.brewmaster.Entity.BrewIteration;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class BrewIterationService {
    private ArrayList<BrewIteration> brewIterations;
    DatabaseReference mDatabase;


    public BrewIterationService(String user) {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(user);
        brewIterations = new ArrayList<>();
    }

    public List<BrewIteration> GetBrewIterations(final BrewIterationAdapter adapter){

        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    BrewIteration iteration = dataSnapshot.getValue(BrewIteration.class);
                    adapter.add(iteration);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return this.brewIterations;
    }

    public void GetCurrentIteration(final CurrentBrewIterationListener brewIterationListener){
        this.brewIterations = new ArrayList<BrewIteration>();
        mDatabase.child("startDate").orderByChild("time");

        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    BrewIteration iteration = snapshot.getValue(BrewIteration.class);
                    brewIterations.add(iteration);
                }
                brewIterationListener.recieveCurrentBrewIteration(brewIterations.get(0));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        mDatabase.addListenerForSingleValueEvent(eventListener);


    }

    public void AddBrewIteration(BrewIteration brewIteration){
        mDatabase.child(brewIteration.getName()).setValue(brewIteration);
        mDatabase.push();
    }




}
