package com.example.thebrewmangroup.brewmaster;

import android.arch.core.util.Function;

import com.example.thebrewmangroup.brewmaster.Entity.BrewData;
import com.example.thebrewmangroup.brewmaster.Entity.BrewIteration;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

public class BrewIterationDataService {
    DatabaseReference mDatabase;
    List<BrewData> dataArray;
    String sensorId;

    public BrewIterationDataService(String id) {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        dataArray = new ArrayList<BrewData>();
        this.sensorId = id;
    }

    public void GetBrewData(final BrewIteration brewIteration, final BrewDataListener brewListener) {

        if (brewIteration.getStartDate() == null ) {
            brewIteration.setStartDate(new Date());
        }
        mDatabase = FirebaseDatabase.getInstance().getReference().child("sensors").child(sensorId);
        mDatabase.orderByChild("timestamp").startAt(brewIteration.getStartDate().getTime());
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<BrewData> datalist = new ArrayList<>();
                if(brewIteration.getStartDate() == null)
                    brewIteration.setStartDate(new Date());
                if(brewIteration.getEndDate() == null)
                    brewIteration.setEndDate(new Date());

                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    BrewData detail = snapshot.getValue(BrewData.class);
                    if(detail.getTimestamp() == null)
                    {
                        detail.setTimestamp(new Date().getTime());
                    }
                    if(detail.getTimestamp().before(brewIteration.getEndDate()) && detail.getTimestamp().after(brewIteration.getStartDate())) {
                        datalist.add(detail);
                    }
                }
                brewListener.recieveData(brewIteration, datalist);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
