package com.example.thebrewmangroup.brewmaster;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;

public class BrewActivity extends AppCompatActivity {

    private String[] brewIteration = { "First Iteration", "Second Iteration"};
    private BaseAdapter arrayAdapter;
    private ListView listView;
        // replace with dataprovider
    private BrewIterationService brewIterationService = new BrewIterationService("myuser");
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_current:
                    Intent intent = new Intent(BrewActivity.this    , MainActivity.class);
                    startActivity(intent);
                    return true;
                case R.id.navigation_history:
                    Intent historyIntent = new Intent(BrewActivity.this    , BrewActivity.class);
                    startActivity(historyIntent);
                    return  true;
            }
            return false;
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brew);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        this.brewIterationService = new BrewIterationService("myuser");
        this.listView =  (ListView) findViewById(R.id.brew_iteration);
        //this.arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, brewIterationService.getData());
        //this.arrayAdapter = new BrewIterationAdapter(this,this.brewIterationService.getData());
        this.listView.setAdapter(arrayAdapter);
    }
}
