package com.example.thebrewmangroup.brewmaster;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.thebrewmangroup.brewmaster.Entity.BrewData;
import com.example.thebrewmangroup.brewmaster.Entity.BrewIteration;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class BrewIterationDetail extends Fragment {

    private static final String BREW_ITERATION_KEY = "brew_iteration";

    private OnFragmentInteractionListener mListener;
    private EditText nameText;
    private EditText descriptionText;
    private BrewIterationDataService service;
    private Button startStopButton;
    private BrewIteration brewIteration;
    private View v;
    private BrewIterationService brewService;

    public BrewIterationDetail() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public BrewIterationDetail(BrewIteration brewIteration) {
        this.brewIteration = brewIteration;
    }

    public static BrewIterationDetail newInstance(BrewIteration brewIteration) {
        BrewIterationDetail fragment = new BrewIterationDetail();
        Bundle arguments = new Bundle();
        arguments.putSerializable(BREW_ITERATION_KEY, brewIteration);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_brew_iteration_detail, container, false);
        brewService = new BrewIterationService("myuser");
        nameText = (EditText) v.findViewById(R.id.brewName);
        descriptionText = (EditText) v.findViewById(R.id.brewDesciption);
        startStopButton = (Button) v.findViewById(R.id.brewStartStopButton);



        final BrewIteration selectedIteration = (BrewIteration) (getArguments() != null ? getArguments().getSerializable(BREW_ITERATION_KEY) : null);
        if (selectedIteration == null) {
            brewService.GetCurrentIteration(new CurrentBrewIterationListener() {
                @Override
                public void recieveCurrentBrewIteration(BrewIteration iteration) {
                    brewIteration = iteration;
                    displayData(iteration);
                     handleStartStopButton(iteration);
                }
            });
        } else
        {
            handleStartStopButton(selectedIteration);
            displayData(selectedIteration);
        }


        TextWatcher nameWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
            }
        };

        TextWatcher descriptionWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //brewIteration.setDescription(s.toString());
                //TODO Firebase Shissel
                if(brewIteration == null) {
                    brewIteration = new BrewIteration();
                }
                brewIteration.setDescription(s.toString());
                brewIteration.setName(nameText.getText().toString());
                brewService.AddBrewIteration(brewIteration);
            }
        };

        nameText.addTextChangedListener(nameWatcher);
        descriptionText.addTextChangedListener(descriptionWatcher);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    private void handleStartStopButton(final BrewIteration brewiteration) {
        if(brewiteration.getStartDate() == null)
        {
            startStopButton.setText("Start");
            startStopButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    brewiteration.setStartDate(new Date());
                    brewService.AddBrewIteration(brewiteration);
                    handleStartStopButton(brewiteration);
                }
            });
        }
        else if(brewiteration.getEndDate() == null){
            startStopButton.setText("Stop");
            startStopButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    brewiteration.setEndDate(new Date());
                    brewService.AddBrewIteration(brewiteration);
                    handleStartStopButton(brewiteration);
                }
            });
        }
        else {
            startStopButton.setEnabled(false);
        }
    }

    public interface OnFragmentInteractionListener {

    }

    private void displayData(BrewIteration selectedIteration) {
        nameText.setText(selectedIteration.getName());
        descriptionText.setText(selectedIteration.getDescription());

        service = new BrewIterationDataService("x71");
        List<BrewIterationDetail> data;
        if(selectedIteration.getStartDate() != null) {
            service.GetBrewData(selectedIteration, new BrewDataListener() {
                @Override
                public void recieveData(BrewIteration iteration, List<BrewData> data) {
                    if (data == null || data.size() == 0)
                        return;

                    GraphView graph = (GraphView) v.findViewById(R.id.brewGraph);
                    LineGraphSeries<DataPoint> series = new LineGraphSeries<>();
                    //Get min & max or order it
                    Collections.sort(data, new Comparator<BrewData>() {
                        @Override
                        public int compare(BrewData o1, BrewData o2) {
                            return o1.getTimestamp().compareTo(o2.getTimestamp());
                        }
                    });
                    BrewData first = data.get(0);
                    BrewData last = data.get(data.size()-1);

                    for (BrewData brewData : data) {
                        series.appendData(new DataPoint(brewData.getTimestamp(), brewData.getValue()), true, 2000);
                    }
                    graph.addSeries(series);

                    // set date label formatter
                    graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(v.getContext()));
                    graph.getGridLabelRenderer().setNumHorizontalLabels(3);
                    graph.getViewport().setMinX(first.getTimestamp().getTime());
                    graph.getViewport().setMaxX(last.getTimestamp().getTime());
                    graph.getViewport().setXAxisBoundsManual(true);
                    graph.getGridLabelRenderer().setHumanRounding(true);

                }
            });
        }
    }
}
