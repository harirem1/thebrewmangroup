package com.example.thebrewmangroup.brewmaster;

import com.example.thebrewmangroup.brewmaster.Entity.BrewIteration;

import java.util.List;

public interface BrewIterationListener {
    public void recieveBrewIteration(List<BrewIteration> iteration);
}
