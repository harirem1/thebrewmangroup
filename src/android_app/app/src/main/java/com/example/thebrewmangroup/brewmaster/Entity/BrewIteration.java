package com.example.thebrewmangroup.brewmaster.Entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by remo.hari on 27.04.2018.
 */

public class BrewIteration implements Serializable{

    private String name;
    private String description;
    private Date startDate, endDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
